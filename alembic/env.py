import logging
from logging.config import fileConfig

from sqlalchemy import engine_from_config, pool

from alembic import context
from src.base_class import Base
from src.config import settings
from src.diet.models import *

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Override default DB connection string
config.set_main_option("sqlalchemy.url", settings.DATABASE_URL)
config.compare_type = True
config.compare_server_default = True
# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

log = logging.getLogger(__name__)
# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


# don't create empty revisions
def process_revision_directives(context, revision, directives):
    script = directives[0]
    if script.upgrade_ops.is_empty():
        directives[:] = []
        log.warning("No changes found skipping revision creation.")


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        config.get_section(config.config_ini_section, {}),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            process_revision_directives=process_revision_directives,
            include_schemas=True,
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    log.warning("Can't run migrations offline")
else:
    run_migrations_online()
