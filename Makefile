args = "$(filter-out $@,$(MAKECMDGOALS))"
dc = docker compose -f docker-compose.yml

build-dev: requirements-dev _build
build-prod: requirements _build

_build:
	docker compose build

migrate:
	docker compose run api alembic upgrade head

up:
	$(dc) up -d db
	$(dc) up api --remove-orphans

down:
	$(dc) down

revision:
	docker compose run api alembic revision -m $(call args) --autogenerate

downgrade:
	poetry run alembic downgrade $(msg)

black:
	poetry run black src

isort:
	poetry run isort src --profile black

autoflake:
	poetry run autoflake --in-place --remove-all-unused-imports --remove-unused-variables --recursive src --exclude=__init__.py

format: autoflake isort black

requirements-dev:
	poetry export -f requirements.txt --output requirements/dev.txt --with dev

requirements:
	poetry export -f requirements.txt --output requirements/prod.txt
