import logging
from abc import ABC, abstractmethod

import requests

from src.config import settings

log = logging.getLogger(__name__)


class EmailClient(ABC):
    @abstractmethod
    def send(self, *, subject: str, recipients: list[str], text: str) -> bool:
        raise NotImplementedError

    @property
    @abstractmethod
    def from_email(self) -> str:
        raise NotImplementedError


class ConsoleEmailClient(EmailClient):
    @property
    def from_email(self) -> str:
        return "Bob <test@email.com>"

    def send(self, *, subject: str, recipients: list[str], text: str) -> bool:
        log.info(
            f"{self.__class__}\nfrom: {self.from_email}\nto: {recipients}\nsubject: {subject}\n--------\n{text}"
        )
        return True


class MailgunClient(EmailClient):
    @property
    def api_key(self) -> str:
        return settings.MAILGUN_API_KEY

    @property
    def from_email(self) -> str:
        return f"{settings.CONTACT_NAME} <mailgun@{settings.MAILGUN_DOMAIN}>"

    @property
    def url(self) -> str:
        return (
            f"https://api.mailgun.net/v3/{settings.MAILGUN_DOMAIN}.mailgun.org/messages"
        )

    def send(self, *, subject: str, recipients: list[str], text: str) -> bool:
        try:
            response = requests.post(
                self.url,
                auth=("api", self.api_key),
                data={
                    "from": self.from_email,
                    "to": recipients,
                    "subject": subject,
                    "text": text,
                },
            )
        except:
            log.exception("[Mailgun] Cannot send a request")
            return False

        if not response.ok:
            log.exception(
                f"[Mailgun] http status {response.status_code} - {response.json()}"
            )

        return response.ok
