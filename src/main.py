import logging

import sentry_sdk
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from src.config import app_configs, settings
from src.diet.admin import router as admin_diet_router
from src.diet.router import router as diet_router

app = FastAPI(**app_configs)


@app.on_event("startup")
def setup_ini():
    logging.config.fileConfig("logging.ini", disable_existing_loggers=False)


app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.CORS_ORIGINS,
    allow_origin_regex=settings.CORS_ORIGINS_REGEX,
    allow_credentials=True,
    allow_methods=("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"),
    allow_headers=settings.CORS_HEADERS,
)

if settings.ENVIRONMENT.is_deployed:
    sentry_sdk.init(
        dsn=settings.SENTRY_DSN,
        environment=settings.ENVIRONMENT,
    )


@app.get("/healthcheck", include_in_schema=False)
async def healthcheck() -> dict[str, str]:
    return {"status": "ok"}


# admin pages
app.include_router(admin_diet_router, prefix="/api/admin/diet", tags=["Admin Diet"])

# views
app.include_router(diet_router, prefix="/api/diet", tags=["Diet"])
