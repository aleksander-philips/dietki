import datetime
import logging
import os
import re
from collections import defaultdict
from decimal import Decimal
from itertools import groupby

from pdfminer.high_level import extract_text
from sqlalchemy.orm import Session, load_only

from src.clients import EmailClient
from src.config import settings
from src.diet.constants import DAYS, OBIAD, SHOPPING_LIST_SUBJECT
from src.diet.models import Diet, Ingredient, Meal
from src.diet.schemas import (
    AllMealsSchema,
    DaySchema,
    ExportInputSchema,
    IngredientSchema,
    MealSchema,
    PersonMealsSchema,
)

log = logging.getLogger(__name__)


async def load_data(db: Session) -> None:
    service = PdfToPython()
    for filename in os.listdir("./documents"):
        if filename[-3:] != "pdf":
            continue
        log.info(f"load-data: {filename}")
        service.create_diet(db, f"./documents/{filename}")


async def dinner_list(db: Session) -> list[Meal]:
    meals = (
        db.query(Meal)
        .options(load_only(Meal.diet_id))
        .join(Diet)
        .filter(
            Meal.time == OBIAD,
            # Meal.name.ilike("%e penne po bol%"),
            Diet.person_name == settings.PERSON_NAME,
        )
        .order_by(Meal.name)
        .all()
    )
    return meals


async def get_all_meals(db: Session, diet_id: int) -> AllMealsSchema:
    diet = db.query(Diet).get(diet_id)
    other_diets = (
        db.query(Diet)
        .filter(Diet.version == diet.version, Diet.id.not_in([diet_id]))
        .all()
    )

    person_meals = list()
    diets = [diet, *other_diets]
    diets.sort(key=lambda x: x.person_name)
    for diet in diets:
        sorted_meals = {
            day: list(key) for day, key in groupby(diet.meals, lambda x: x.day)
        }
        days = list()
        for day, meals_obj in sorted_meals.items():
            meals = list()
            for meal in meals_obj:
                ingredients = list()
                for ingredient in meal.ingredients:
                    ingredients.append(IngredientSchema(**ingredient.__dict__))
                meals.append(
                    MealSchema(
                        meal_id=meal.id,
                        time=meal.time,
                        day=meal.day,
                        name=meal.name,
                        ingredients=ingredients,
                    )
                )
            days.append(DaySchema(day=day, meals=meals))
        person_meals.append(PersonMealsSchema(person_name=diet.person_name, days=days))

    return AllMealsSchema(version=diet.version, person_meals=person_meals)


async def export_meals(db: Session, data: list[ExportInputSchema]) -> dict:
    ingredients = defaultdict(lambda: dict(weight=0, quantity=0))
    summary = defaultdict(list)
    for item in data:
        meals = db.query(Meal).join(Diet).filter(Meal.id.in_(item.meals)).all()
        for meal in meals:
            for ingredient in meal.ingredients:
                multiplier = item.multiplier

                weight = ingredient.weight * multiplier
                quantity = ingredient.quantity * multiplier
                values = ingredients[ingredient.name]
                ingredients[ingredient.name] = dict(
                    weight=values["weight"] + weight,
                    quantity=values["quantity"] + quantity,
                    measure=ingredient.measure,
                )

            if meal.time == OBIAD:
                summary[meal.diet.person_name].append(
                    dict(
                        dinner_name=meal.name,
                        multiplier=multiplier,
                        version=meal.diet.version,
                    )
                )

    return dict(summary=summary, ingredients=ingredients)


async def send_shopping_list(email_client: EmailClient, data: dict) -> bool:
    summary_text = ""
    for person_name, dinners in data["summary"].items():
        summary_text += f"{person_name}:\n"
        summary_text += "\n".join(
            [
                f"- {x['dinner_name']} x {x['multiplier']} ({x['version']})"
                for x in dinners
            ]
        )
        summary_text += "\n\n"

    ingredients_text = "\n".join(
        [
            f"{key} {x['weight']}g ({x['quantity']} x {x['measure']})"
            for key, x in data["ingredients"].items()
        ]
    )

    return email_client.send(
        subject=f"{SHOPPING_LIST_SUBJECT} {datetime.date.today()}",
        recipients=settings.SHOPPING_LIST_RECIPIENTS,
        text=f"{summary_text}\n{ingredients_text}",
    )


class PdfToPython:
    def create_diet(self, db: Session, path: str) -> None:
        text = extract_text(path)
        person_name, version = path.split("/")[-1].split()
        version = int(version.split(".")[0])
        query = (
            db.query(Diet).filter_by(person_name=person_name, version=version).exists()
        )
        if db.query(query).scalar():
            log.info(f"load-data: skipped {person_name} {version}.pdf")
            return

        diet = Diet(person_name=person_name, version=version, path=path)
        db.add(diet)
        db.flush()
        self._collect_days(db, diet.id, text)
        db.flush()
        self._assert_diet(diet)
        db.commit()

    def _collect_ingredients(
        self, db: Session, meal_id: int, recipe: str
    ) -> list[Ingredient]:
        ingredient_pattern = (
            r"([ \w,%\(\)\"']+) +- *(\d+) *g *\((\d+.?\d?) +x *([ \w]+)\)"
        )
        ingredients = recipe.split("\n")
        ingredients = [x for x in ingredients if x]

        for ingredient_text in ingredients[1:]:
            parsed = re.match(ingredient_pattern, ingredient_text)
            try:
                ingredient = Ingredient(
                    name=parsed[1],
                    weight=int(parsed[2]),
                    quantity=Decimal(parsed[3]),
                    measure=parsed[4],
                    meal_id=meal_id,
                )
                db.add(ingredient)
            except Exception:
                continue

    def _collect_recipe(
        self,
        db: Session,
        day: str,
        diet_id: int,
        name: str,
        day_text: str,
        start: str,
        stop: str,
    ) -> None:
        recipe_pattern = start + r" +\d{2}:\d{2}(.+)"
        if stop:
            recipe_pattern += stop + r" +\d{2}:\d{2}"
        recipe = re.search(recipe_pattern, day_text, re.S)[1]
        meal = Meal(
            day=day,
            time=start.lower(),
            name=name.lower(),
            diet_id=diet_id,
        )
        db.add(meal)
        db.flush()

        self._collect_ingredients(db, meal.id, recipe)

    def _collect_meals(
        self, db: Session, day: str, diet_id: int, day_text: str
    ) -> None:
        time_pattern = r"\n\n(\w+[ ]?\w+) \d{2}:\d{2}"
        name_pattern = (
            r"\d{2}:\d{2}\n\n[\s\S]*?([A-ZŁĄĘŻŹĆÓŃŚ][ ŁĄĘŻŹĆÓŃŚA-Z\"'`,\(\)-]+)"
        )

        times = re.findall(time_pattern, day_text)
        names = re.findall(name_pattern, day_text)
        times.append("")  # stop for the last meal

        for i, time in enumerate(times[:-1]):
            name = names.pop(0)
            self._collect_recipe(db, day, diet_id, name, day_text, time, times[i + 1])

    def _collect_days(
        self, db: Session, diet_id: int, text: str, missing_first_page: bool = False
    ) -> None:
        if missing_first_page:
            day_pattern = "{start}(.*){stop}"
        else:
            day_pattern = "{start}.*{start}(.*){stop}"
        for i, day in enumerate(DAYS[:-1]):
            day_text = re.search(
                day_pattern.format(start=day, stop=DAYS[i + 1]), text, re.S
            )
            try:
                day_text = day_text[1].replace("\x0c", "")
            except TypeError:
                return self._collect_days(db, diet_id, text, True)

            self._collect_meals(db, day, diet_id, day_text)

    def _assert_diet(self, diet: Diet) -> None:
        meals_number = len(diet.meals)
        assert meals_number == 35, f"found only {meals_number} meals"
