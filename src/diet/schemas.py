from pydantic import BaseModel, conlist


class DinnerSchema(BaseModel):
    class Config:
        orm_mode = True

    name: str
    diet_id: int


class IngredientSchema(BaseModel):
    name: str
    weight: int
    quantity: float
    measure: str


class MealSchema(BaseModel):
    meal_id: int
    time: str
    name: str

    ingredients: list[IngredientSchema]


class DaySchema(BaseModel):
    day: str
    meals: list[MealSchema]


class PersonMealsSchema(BaseModel):
    person_name: str
    days: list[DaySchema]


class AllMealsSchema(BaseModel):
    version: int

    person_meals: list[PersonMealsSchema]


class ExportInputSchema(BaseModel):
    multiplier: int
    meals: conlist(int, min_items=1)


class IngredientExportSchema(BaseModel):
    weight: int
    quantity: float
    measure: str


class SummaryDaysSchema(BaseModel):
    dinner_name: str
    multiplier: str


class SummarySchema(BaseModel):
    __root__: list[SummaryDaysSchema]


class ExportSchema(BaseModel):
    summary: dict[str, SummarySchema]
    ingredients: dict[str, IngredientExportSchema]
