import sqlalchemy as sa
from sqlalchemy import func
from sqlalchemy.orm import Mapped, relationship

from src.base_class import Base


class Diet(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    version = sa.Column(sa.Integer, nullable=False)
    person_name = sa.Column(sa.String, nullable=False)
    created_at = sa.Column(sa.DateTime, server_default=func.now())
    path = sa.Column(sa.String, nullable=False, unique=True)

    meals: Mapped[list["Meal"]] = relationship(back_populates="diet")

    __table_args__ = (
        sa.UniqueConstraint("version", "person_name", name="_version_person_name_uc"),
    )


class Meal(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    time = sa.Column(sa.String, nullable=False)
    day = sa.Column(sa.String, nullable=False)
    name = sa.Column(sa.String, nullable=False)
    diet_id = sa.Column(sa.ForeignKey(Diet.id), nullable=False)

    diet: Mapped["Diet"] = relationship(back_populates="meals")
    ingredients: Mapped[list["Ingredient"]] = relationship()


class Ingredient(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String, nullable=False)
    weight = sa.Column(sa.Integer, nullable=False)
    quantity = sa.Column(sa.Float, nullable=False)
    measure = sa.Column(sa.String, nullable=False)
    meal_id = sa.Column(sa.ForeignKey(Meal.id), nullable=False)
