from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from src.dependencies import get_db
from src.diet.service import load_data

router = APIRouter()


@router.post("/load-data", response_model=None)
async def load_data_view(db: Session = Depends(get_db)):
    await load_data(db)
