from fastapi import APIRouter, Depends
from pydantic import conlist
from pytest import Session

from src.dependencies import get_db, get_email_client
from src.diet.schemas import (
    AllMealsSchema,
    DinnerSchema,
    ExportInputSchema,
    ExportSchema,
)
from src.diet.service import (
    dinner_list,
    export_meals,
    get_all_meals,
    send_shopping_list,
)

router = APIRouter()


@router.get("/{diet_id}/all-meals", response_model=AllMealsSchema)
async def get_all_meals_view(diet_id: int, db: Session = Depends(get_db)):
    return await get_all_meals(db, diet_id)


@router.get("/dinner-list", response_model=list[DinnerSchema])
async def dinner_list_view(db: Session = Depends(get_db)):
    return await dinner_list(db)


@router.post("/export", response_model=ExportSchema)
async def export_view(
    input: conlist(ExportInputSchema, min_items=1),
    db: Session = Depends(get_db),
    email_client=Depends(get_email_client),
):
    result = await export_meals(db, input)
    await send_shopping_list(email_client, result)
    return result
