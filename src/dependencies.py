from typing import Generator

from src.clients import ConsoleEmailClient, EmailClient, MailgunClient
from src.config import settings
from src.session import SessionLocal


def get_db() -> Generator:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_email_client() -> EmailClient:
    if settings.ENVIRONMENT.is_development:
        return ConsoleEmailClient()
    return MailgunClient()
